package com.pnitram.rongiajadv3;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Martin-PC on 11-Jun-16.
 */
public class CardBalanceFragment extends Fragment {

    private static final String API_URL = "http://knjs.xyz:8080/cardBalance?cardNumber=";

    TextView tvCardNumber;
    TextView tvCardBalance;
    Button btnRefreshBalance;
    ProgressDialog progressDialog;
    private MainActivity mainActivity;
    getCardBalance getCardBalanceTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(container == null) {
            return null;
        }
        mainActivity = ((MainActivity)getActivity());

        LinearLayout ll = (LinearLayout)inflater.inflate(R.layout.fragment_cardbalance, container, false);

        tvCardNumber = (TextView)ll.findViewById(R.id.tvCardNumber);
        tvCardBalance = (TextView)ll.findViewById(R.id.tvCardBalance);
        btnRefreshBalance = (Button) ll.findViewById(R.id.btnRefreshBalance);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getString(R.string.query_server));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(true);

        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                getCardBalanceTask.cancel(true);
            }
        });

        tvCardNumber.setText(mainActivity.sharedPreferences.getString(mainActivity.cardNumberPrefKey, mainActivity.cardNumberDefault));

        btnRefreshBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!tvCardNumber.getText().toString().isEmpty()) {
                    if(mainActivity.isInternetAvailable()) {
                        getCardBalanceTask = new getCardBalance();
                        getCardBalanceTask.execute(API_URL+tvCardNumber.getText().toString());
                    }
                } else {
                    mainActivity.makeToast(getString(R.string.malformed_cardnubmer));
                }
            }
        });

        return ll;
    }

    public void setTvCardNumber(String s) {
        tvCardNumber.setText(s);
    }

    class getCardBalance extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... urls) {
            HttpURLConnection conn = null;
            InputStream stream;
            BufferedReader rdr = null;
            StringBuilder builder;

            try {
                URL url = new URL(urls[0]);
                conn = (HttpURLConnection)url.openConnection();
                conn.connect();

                stream = conn.getInputStream();
                rdr = new BufferedReader(new InputStreamReader(stream));
                builder = new StringBuilder();

                String line;
                while((line = rdr.readLine()) != null) {
                    builder.append(line);
                }
                return builder.toString();
            } catch (IOException e) {
                System.out.println(e);
            } finally {
                if(conn != null) {
                    conn.disconnect();
                }
                try {
                    if(rdr != null) {
                        rdr.close();
                    }
                } catch (IOException ex) {
                    System.out.println(ex);
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result != null) {
                try {
                    JSONObject json = new JSONObject(Html.fromHtml(result).toString());
                    if(json.getString("result").equals("success")) {
                        tvCardBalance.setText(String.format("%s %s%s", getString(R.string.card_balance), json.getString("cardbalance"), getString(R.string.euro)));
                    }else{
                        mainActivity.makeToast(getString(R.string.card_number_doesnt_exist));
                    }
                } catch (JSONException e) {
                    System.out.println(e);
                }
            }else{
                System.out.println(getString(R.string.result_null));
                mainActivity.makeToast(getString(R.string.result_null));
            }
            progressDialog.dismiss();
        }
    }
}