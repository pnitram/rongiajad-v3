package com.pnitram.rongiajadv3;

import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Martin-PC on 14-Jun-16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static String DB_NAME = "timetable.sqlite";
    private static String DB_PATH;

    private SQLiteDatabase myDb;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
        DB_PATH = context.getFilesDir()+"/"+DB_NAME;
    }

    public void openDatabase() throws SQLException {
        myDb = SQLiteDatabase.openDatabase(DB_PATH, null, SQLiteDatabase.OPEN_READONLY);
    }

    public String getLastUpdate() {
        Cursor cursor = null;
        try {
            cursor = myDb.rawQuery("SELECT * FROM last_update WHERE id='1'", null);
            try {
                cursor.moveToFirst();
                return cursor.getString(cursor.getColumnIndexOrThrow("date"));
            } finally {
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    public List<String> getTrainStops() {
        Cursor cursor = null;
        try {
            List<String> stopList = new ArrayList<>();
            cursor = myDb.rawQuery("SELECT * FROM stops", null);
            try {
                while(cursor.moveToNext()) {
                    String stop_name = cursor.getString(cursor.getColumnIndexOrThrow("stop_name"));
                    stopList.add(capitalizeString(stop_name));
                }
                return stopList;
            } finally {
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    public List<TripModel> getTrainTimes(String stopA, String stopB) {
        int type;
        if(compareStopID(stopA, stopB)) {
            type = 1;
        } else {
            type = 2;
        }

        List<TripModel> tripList = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = myDb.rawQuery("SELECT * FROM stop_times WHERE stop_id='"+getStopID(stopA, type)+"' ORDER BY departure_time ASC", null);
            cursor.moveToFirst();
            do {
                if(checkTripEndExists(cursor.getInt(cursor.getColumnIndexOrThrow("trip_id")), getStopID(stopB, type))) {
                    String tripStart = cursor.getString(cursor.getColumnIndexOrThrow("departure_time"));
                    String tripEnd = getTripEnd(cursor.getInt(cursor.getColumnIndexOrThrow("trip_id")), getStopID(stopB, type));

                    tripList.add(new TripModel(tripStart.substring(0, tripStart.length() - 3), tripEnd.substring(0, tripEnd.length() - 3)));
                }
            } while (cursor.moveToNext());
            cursor.close();
            return tripList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return Collections.emptyList();
    }

    private String getTripEnd(int trip_id, int stop_id) {
        String tripEnd;

        Cursor cursor = null;
        try {
            cursor = myDb.rawQuery("SELECT * FROM stop_times WHERE trip_id='"+trip_id+"' AND stop_id='"+stop_id+"' ORDER BY departure_time ASC", null);
            cursor.moveToFirst();
            tripEnd = cursor.getString(cursor.getColumnIndexOrThrow("departure_time"));
            return tripEnd;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    private boolean checkTripEndExists(int trip_id, int stop_id) {
        Cursor cursor = myDb.rawQuery("SELECT * FROM stop_times WHERE trip_id='"+trip_id+"' AND stop_id='"+stop_id+"' ORDER BY departure_time ASC", null);
        if(cursor.getCount() == 0) {
            cursor.close();
            return false;
        } else {
            cursor.close();
            return true;
        }
    }

    private int getStopID(String stopname, int type) {
        int stopID = 0;

        Cursor cursor = null;

        try {
            cursor = myDb.rawQuery("SELECT * FROM stops WHERE stop_name='"+stopname+"'", null);
            cursor.moveToFirst();
            if(type == 1) {
                stopID = cursor.getInt(cursor.getColumnIndexOrThrow("stop_id"));
            } else if(type == 2) {
                stopID = cursor.getInt(cursor.getColumnIndexOrThrow("stop_id2"));
            }
            cursor.close();

            return stopID;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(cursor != null){
                cursor.close();
            }
        }
        return 0;
    }

    private boolean compareStopID(String stopA, String stopB) {
        try {
            Cursor cursor;
            int stopA_id;
            int stopB_id;

            cursor = myDb.rawQuery("SELECT * FROM stops WHERE stop_name='"+stopA+"'", null);
            cursor.moveToFirst();
            stopA_id = cursor.getInt(cursor.getColumnIndexOrThrow("id"));
            cursor.close();

            cursor = myDb.rawQuery("SELECT * FROM stops WHERE stop_name='"+stopB+"'", null);
            cursor.moveToFirst();
            stopB_id = cursor.getInt(cursor.getColumnIndexOrThrow("id"));
            cursor.close();

            return stopA_id < stopB_id;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private String capitalizeString(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
