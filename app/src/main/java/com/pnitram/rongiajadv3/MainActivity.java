package com.pnitram.rongiajadv3;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraDevice;
import android.media.audiofx.BassBoost;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.jar.Manifest;

public class MainActivity extends AppCompatActivity {

    private static final String FILENAME = "timetable.sqlite";

    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    SectionsPagerAdapter pagerAdapter;
    ProgressDialog progressDialog;
    downloadDatabaseFile downloadDatabaseFileTask;
    public SharedPreferences sharedPreferences;
    public String cardNumberPrefKey;
    public String cardNumberDefault;
    boolean downloadingDb;
    public DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        tabLayout = (TabLayout)findViewById(R.id.tab_layout);
        viewPager = (ViewPager)findViewById(R.id.view_pager);
        pagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());

        sharedPreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        cardNumberPrefKey = "cardnumber";
        cardNumberDefault = "XXXXXXXXXXX";

        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setupWithViewPager(viewPager);
        toolbar.setTitle(R.string.app_name);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.downloading_database));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(true);

        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                downloadDatabaseFileTask.cancel(true);
            }
        });

        setSupportActionBar(toolbar);
        checkAndRequestPermissions();

        dbHelper = new DatabaseHelper(this);

        if(!doesDbFileExist()) {
            openDatabaseDialog(getString(R.string.database_missing_title), getString(R.string.database_missing_message));
        } else {
            dbHelper.openDatabase();
        }
    }

    private void getDatabase() {
        if(isInternetAvailable()) {
            File file = this.getFileStreamPath(FILENAME);
            if(!file.exists()) {
                if(!downloadingDb) {
                    downloadDatabaseFileTask = new downloadDatabaseFile();
                    downloadDatabaseFileTask.execute("http://knjs.xyz/"+FILENAME);
                } else {
                    makeToast(getString(R.string.already_downloading));
                }
            } else {
                new checkForDatabaseUpdate().execute("http://knjs.xyz/test.php");
            }
        }
    }

    public boolean doesDbFileExist() {
        return getApplicationContext().getFileStreamPath(FILENAME).exists();
    }

    public void openDatabaseDialog(String title, String msg) {
        final AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle(title);
        b.setMessage(msg);
        b.setPositiveButton(getString(R.string.alert_ok), null);
        b.setNegativeButton(getString(R.string.alert_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final AlertDialog dialog = b.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDatabase();
                dialog.dismiss();
            }
        });
    }

    private void openCardNumberDialog() {
        final AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle(getString(R.string.card_number));
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setGravity(Gravity.CENTER);
        String cardnumber;
        if(!(cardnumber = sharedPreferences.getString(cardNumberPrefKey, cardNumberDefault)).equals(cardNumberDefault)) {
            input.setText(cardnumber);
        }
        b.setView(input);
        b.setPositiveButton(getString(R.string.alert_ok), null);
        b.setNegativeButton(getString(R.string.alert_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final AlertDialog dialog = b.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(input.getText().toString().length() >= 11 && input.getText().toString().length() <= 16) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(cardNumberPrefKey, input.getText().toString());
                    editor.apply();

                    CardBalanceFragment cfb = (CardBalanceFragment)pagerAdapter.fragments.get(1);
                    cfb.setTvCardNumber(sharedPreferences.getString(cardNumberPrefKey, cardNumberDefault));

                    dialog.dismiss();
                } else {
                    makeToast(getString(R.string.malformed_cardnubmer));
                }
            }
        });
    }

    public boolean checkAndRequestPermissions() {
        int permissionInternet = ContextCompat.checkSelfPermission(this, android.Manifest.permission.INTERNET);
        int permissionNetworkState = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_NETWORK_STATE);

        List<String> permissionsNeeded = new ArrayList<>();
        if(permissionInternet != PackageManager.PERMISSION_GRANTED) {
            permissionsNeeded.add(android.Manifest.permission.INTERNET);
        }
        if(permissionNetworkState != PackageManager.PERMISSION_GRANTED) {
            permissionsNeeded.add(android.Manifest.permission.ACCESS_NETWORK_STATE);
        }

        if(!permissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, permissionsNeeded.toArray(new String[permissionsNeeded.size()]), 1);
            return false;
        }
        return true;
    }

    public boolean isInternetAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm.getActiveNetworkInfo() == null) {
            makeToast(getString(R.string.no_internet));
        }
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_change_card_number) {
            openCardNumberDialog();
            return true;
        }
        if(id == R.id.action_update_db) {
            getDatabase();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void makeToast(String s) {
        if(s.length() > 15) {
            Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        }
    }

    class checkForDatabaseUpdate extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected String doInBackground(String... urls) {
            HttpURLConnection conn = null;
            InputStream stream;
            BufferedReader rdr = null;
            StringBuilder builder;

            try {
                URL url = new URL(urls[0]);
                conn = (HttpURLConnection)url.openConnection();
                conn.connect();

                stream = conn.getInputStream();
                rdr = new BufferedReader(new InputStreamReader(stream));
                builder = new StringBuilder();

                String line;
                while((line = rdr.readLine()) != null) {
                    builder.append(line);
                }
                return builder.toString();
            } catch (IOException e) {
                System.out.println(e);
            } finally {
                if(conn != null) {
                    conn.disconnect();
                }
                try {
                    if(rdr != null) {
                        rdr.close();
                    }
                } catch (IOException ex) {
                    System.out.println(ex);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result != null) {
                if(!result.equals(dbHelper.getLastUpdate())) {
                    downloadDatabaseFileTask = new downloadDatabaseFile();
                    downloadDatabaseFileTask.execute("http://knjs.xyz/"+FILENAME);
                } else {
                    makeToast("Already up-to-date");
                }
            }
        }
    }

    class downloadDatabaseFile extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            downloadingDb = true;
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... urls) {
            HttpURLConnection conn = null;
            InputStream input = null;
            OutputStream output = null;

            try {
                URL url = new URL(urls[0]);
                conn = (HttpURLConnection)url.openConnection();
                conn.connect();

                input = conn.getInputStream();
                output = openFileOutput(FILENAME, MODE_PRIVATE);

                int fileLength = conn.getContentLength();

                byte data[] = new byte[4096];
                int length;

                long total = 0;

                while((length = input.read(data)) > 0) {
                    output.write(data, 0, length);
                    if(isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += length;
                    if (fileLength > 0)
                        publishProgress((int) (total * 100 / fileLength));
                }
                output.flush();
                output.close();
                input.close();
                return "success";
            } catch (IOException e) {
                System.out.println(e);
            } finally {
                if(conn != null) {
                    conn.disconnect();
                }
                try {
                    if(output != null) {
                        output.close();
                    }
                    if(input != null) {
                        input.close();
                    }
                } catch (IOException ex) {
                    System.out.println(ex);
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result != null) {
                if(result.equals("success")) {
                    dbHelper.openDatabase();
                    TimetableFragment tf = (TimetableFragment)pagerAdapter.fragments.get(0);
                    tf.populateSpinners(dbHelper.getTrainStops());
                    makeToast(getString(R.string.database_downloaded));
                }
            } else {
                makeToast(getString(R.string.download_failed));
            }
            downloadingDb = false;
            progressDialog.dismiss();
        }
    }
}
