package com.pnitram.rongiajadv3;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Martin-PC on 01-Jul-16.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    List<TripModel> data = Collections.emptyList();

    public RecyclerViewAdapter(Context context, List<TripModel> data) {
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.recyclerview_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TripModel current = data.get(position);
        holder.tvTripStart.setText(current.start);
        holder.tvTripEnd.setText(current.end);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTripStart;
        TextView tvTripEnd;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvTripStart = (TextView)itemView.findViewById(R.id.tvTripStart);
            tvTripEnd = (TextView)itemView.findViewById(R.id.tvTripEnd);
        }
    }
}
