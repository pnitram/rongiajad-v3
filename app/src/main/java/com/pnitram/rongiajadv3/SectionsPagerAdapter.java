package com.pnitram.rongiajadv3;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Martin-PC on 11-Jun-16.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    Context ctx;
    List<Fragment> fragments;

    public SectionsPagerAdapter(Context ctx , FragmentManager fm) {
        super(fm);
        this.ctx = ctx;
        fragments = new ArrayList<>();
        fragments.add(new TimetableFragment());
        fragments.add(new CardBalanceFragment());
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return fragments.get(0);
            case 1:
                return fragments.get(1);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return ctx.getString(R.string.tab1_name);
            case 1:
                return ctx.getString(R.string.tab2_name);
            default:
                return null;
        }
    }
}