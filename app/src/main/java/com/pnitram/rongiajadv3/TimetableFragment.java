package com.pnitram.rongiajadv3;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Martin-PC on 11-Jun-16.
 */
public class TimetableFragment extends Fragment {

    Spinner sStopA;
    Spinner sStopB;
    Button btnSearch;
    private RecyclerView rvTripTimes;
    private MainActivity mainActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        if(container == null) {
            return null;
        }
        mainActivity = ((MainActivity)getActivity());

        RelativeLayout rl = (RelativeLayout)inflater.inflate(R.layout.fragment_timetable, container, false);
        sStopA = (Spinner)rl.findViewById(R.id.sStopA);
        sStopB = (Spinner)rl.findViewById(R.id.sStopB);
        btnSearch = (Button)rl.findViewById(R.id.btnSearch);
        rvTripTimes = (RecyclerView)rl.findViewById(R.id.rvTripTimes);

        rvTripTimes.setLayoutManager(new LinearLayoutManager(getActivity()));

        if(mainActivity.dbHelper != null) {
            populateSpinners(mainActivity.dbHelper.getTrainStops());
        }

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!mainActivity.doesDbFileExist()) {
                    mainActivity.openDatabaseDialog(getString(R.string.database_missing_title), getString(R.string.database_missing_message));
                }

                if(sStopA.getCount() == 0 || sStopB.getCount() == 0) {
                    return;
                }

                String stopA = sStopA.getSelectedItem().toString().toLowerCase();
                String stopB = sStopB.getSelectedItem().toString().toLowerCase();

                if(!stopA.equals(stopB)) {
                    RecyclerViewAdapter adapter = new RecyclerViewAdapter(getContext(), mainActivity.dbHelper.getTrainTimes(stopA, stopB));
                    rvTripTimes.setAdapter(adapter);

                    SharedPreferences.Editor editor = mainActivity.sharedPreferences.edit();
                    editor.putString("stopA", stopA);
                    editor.putString("stopB", stopB);
                    editor.apply();
                }
            }
        });

        return rl;
    }

    public void populateSpinners(List<String> stops) {
        if(sStopA != null && sStopB != null && stops != null) {
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item, stops);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sStopA.setAdapter(spinnerArrayAdapter);
            sStopB.setAdapter(spinnerArrayAdapter);

            sStopA.setSelection(stops.indexOf(capitalizeString(mainActivity.sharedPreferences.getString("stopA", "riisipere"))));
            sStopB.setSelection(stops.indexOf(capitalizeString(mainActivity.sharedPreferences.getString("stopB", "tallinn"))));
        }
    }

    private String capitalizeString(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }
}