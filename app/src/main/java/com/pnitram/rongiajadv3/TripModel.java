package com.pnitram.rongiajadv3;

/**
 * Created by Martin-PC on 01-Jul-16.
 */
public class TripModel {
    String start;
    String end;

    public TripModel(String start, String end) {
        this.start = start;
        this.end = end;
    }

    public String getStart(){return start;}
    public String getEnd(){return end;}

    public void setStart(String start){this.start = start;}
    public void setEnd(String end){this.end = end;}
}